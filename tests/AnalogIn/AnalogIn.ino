int sensorValue = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  sensorValue = analogRead(A0);
  Serial.print(sensorValue);
  Serial.print(" - ");
  Serial.println(map(sensorValue, 0, 940, 1, 10));
  delay(2);
}
