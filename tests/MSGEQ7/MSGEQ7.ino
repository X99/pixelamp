
#include "MSGEQ7.h"
#include <FastLED.h>


#define pinAnalog A2
#define pinReset 3
#define pinStrobe 2
#define MSGEQ7_INTERVAL ReadsPerSecond(50)
#define MSGEQ7_SMOOTH false

CMSGEQ7<MSGEQ7_SMOOTH, pinReset, pinStrobe, pinAnalog> MSGEQ7;

const uint8_t kMatrixWidth = 16;
const uint8_t kMatrixHeight = 8;
uint8_t brightness = 20;
#define NUM_LEDS    (kMatrixWidth * kMatrixHeight)
#define LED_PIN     6
#define COLOR_ORDER GRB
#define CHIPSET     WS2812

CRGB leds[NUM_LEDS];

void setup() {
  // This will set the IC ready for reading
  MSGEQ7.begin();
  Serial.begin(115200);
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(brightness);
  
  FastLED.show();
}

void loop() {
  // Analyze without delay every interval
  bool newReading = MSGEQ7.read(MSGEQ7_INTERVAL);

  // Led output
  fill_solid(leds, NUM_LEDS, 0);
  if (newReading) {
    for (int f=0; f<7; f++) {
      uint8_t input = MSGEQ7.get(f);
      input = mapNoise(input)/16;
      Serial.print(String(input)+" ");
      fill_solid(&leds[kMatrixHeight+2*f*kMatrixHeight], input, CRGB::Violet);
      fill_solid(&leds[2*kMatrixHeight+2*f*kMatrixHeight], input, CRGB::Violet);
    }
    Serial.println();
    FastLED.show();
  }
}
