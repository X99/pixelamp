#include <FastLED.h>

#define NUM_LEDS 128

#define DATA_PIN 6

CRGB leds[NUM_LEDS];

void setup() {
	delay(2000);
  
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
}

void loop() {
  /*leds[0] = CRGB(255,0,0); 
  leds[1] = CRGB(0,255,0);
  leds[2] = CRGB(0,255,0);
  leds[3] = CRGB(0,0,255);
  leds[4] = CRGB(0,0,255);
  leds[5] = CRGB(0,0,255);
  FastLED.show();*/
  
  FastLED.setBrightness(20);

  for (uint8_t i = 0; i<NUM_LEDS; i++) {
    leds[i] = CRGB(100, 0, 100);
    FastLED.show();
    delay(50);
  }

  
  delay(1000);
}
