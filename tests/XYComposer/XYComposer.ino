#include <FastLED.h>

const uint8_t kMatrixWidth = 8;
const uint8_t kMatrixHeight = 8;
const uint8_t kSingleMatrixWidth = 8;

#define NUM_LEDS    (kMatrixWidth * kMatrixHeight)

#define DATA_PIN 6

CRGB leds[NUM_LEDS];


//[0;0] is ont top-left-hand corner
uint16_t translateCoordinates(uint8_t x, uint8_t y, bool wrapX, bool wrapY) {
  const uint8_t matrixMaxX = kMatrixWidth - 1;
  const uint8_t matrixMaxY = kMatrixHeight - 1;
  
  Serial.println("Before: X="+String(x)+" and Y="+String(y));
  if (wrapX == true) {
    while(x > matrixMaxX) x -= kMatrixWidth;
  }
  if (wrapY == true) {
    while(y > matrixMaxY) y -= kMatrixHeight;
  }
  Serial.println("After: X="+String(x)+" and Y="+String(y));

  //due to LEDs being arranged in zigzag
  if (x%2 == 0) {
    y = (kMatrixHeight-1) - y;
  }

  Serial.println("ret="+String((y * kMatrixWidth) + x));
  return (x * kMatrixHeight) + y;
}


void setup() {
  Serial.begin(9600);
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);

  FastLED.setBrightness(20);

  for (uint8_t x = 0; x<kMatrixWidth; x++) {
    for (uint8_t y = 0; y<kMatrixHeight; y++) {
      leds[translateCoordinates(x, y, false, false)] = CRGB(50, 0, 50);
      delay(200);
      FastLED.show();
    }
  }
  return;

  for (uint8_t i = 0; i<NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
  }

  leds[translateCoordinates(0, 0, false, false)] = CRGB(100, 0, 0);
  leds[translateCoordinates(1, 1, false, false)] = CRGB(100, 0, 0);
  FastLED.show();

  

  
  delay(1000);
}

void loop() {
}
