#include <FastLED.h>

/* NeoPixel Shield data pin is always 6. Change for other boards */
#define CONTROL_PIN 6

/* Board shape and size configuration. Sheild is 8x5, 40 pixels */
#define HEIGHT 8
#define WIDTH 16
#define NUM_LEDS HEIGHT*WIDTH

CRGB leds[NUM_LEDS];
CRGBPalette16 gPal;

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<NEOPIXEL, CONTROL_PIN>(leds, NUM_LEDS);
  
  /* Set a black-body radiation palette
     This comes from FastLED */
  gPal = LavaColors_p; 
  
  /* Clear display before starting */
  FastLED.clear();
  FastLED.show();
  FastLED.delay(1500); // Sanity before start
  FastLED.setBrightness(20);
}
const uint8_t kMatrixWidth = 16;
const uint8_t kMatrixHeight = 8;

uint16_t translateCoordinates(uint8_t x, uint8_t y, bool wrapX, bool wrapY) {
  const uint8_t matrixMaxX = kMatrixWidth - 1;
  const uint8_t matrixMaxY = kMatrixHeight - 1;

  //comment this line to put [0;0] on bottom-left-hand corner.
  y = (kMatrixHeight-1) - y;
  
  //Serial.println("Before: X="+String(x)+" and Y="+String(y));
  if (wrapX == true) {
    while(x > matrixMaxX) x -= kMatrixWidth;
  }
  if (wrapY == true) {
    while(y > matrixMaxY) y -= kMatrixHeight;
  }
  //Serial.println("After: X="+String(x)+" and Y="+String(y));

  //uncomment if LEDs are arranged in zigzag
  if (x%2 == 0) {
    //y = (kMatrixHeight-1) - y;
  }

  //Serial.println("ret="+String((y * kMatrixWidth) + x));
  return (x * kMatrixHeight) + y;
}


/* Refresh rate. Higher makes for flickerier
   Recommend small values for small displays */
#define FPS 20
#define FPS_DELAY 1000/FPS

void loop() {
  random16_add_entropy( random() ); // We chew a lot of entropy
  
  Fireplace();

  FastLED.show();
  FastLED.delay(FPS_DELAY); //
}

/* Rate of cooling. Play with to change fire from
   roaring (smaller values) to weak (larger values) */
#define COOLING 55  

/* How hot is "hot"? Increase for brighter fire */
#define HOT 255
#define MAXHOT HOT*HEIGHT

void Fireplace () {
  static unsigned int spark[WIDTH]; // base heat
  CRGB stack[WIDTH][HEIGHT];        // stacks that are cooler
 
  // 1. Generate sparks to re-heat
  for( int i = 0; i < WIDTH; i++) {
    if (spark[i] < HOT ) {
      int base = HOT * 2;
      spark[i] = random16( base, MAXHOT );
    }
  }
  
  // 2. Cool all the sparks
  for( int i = 0; i < WIDTH; i++) {
    spark[i] = qsub8( spark[i], random8(0, COOLING) );
  }
  
  // 3. Build the stack
  /*    This works on the idea that pixels are "cooler"
        as they get further from the spark at the bottom */
  for( int i = 0; i < WIDTH; i++) {
    unsigned int heat = constrain(spark[i], HOT/2, MAXHOT);
    for( int j = HEIGHT-1; j >= 0; j--) {
      /* Calculate the color on the palette from how hot this
         pixel is */
      byte index = constrain(heat, 0, HOT);
      stack[i][j] = ColorFromPalette( gPal, index );
      
      /* The next higher pixel will be "cooler", so calculate
         the drop */
      unsigned int drop = random8(0,HOT);
      if (drop > heat) heat = 0; // avoid wrap-arounds from going "negative"
      else heat -= drop;
 
      heat = constrain(heat, 0, MAXHOT);
    }
  }
  
  // 4. map stacks to led array
  for( int x = 0; x < WIDTH; x++) {
    for( int y = 0; y < HEIGHT; y++) {
      int p = (y*WIDTH) + x;
      //Serial.println("x = "+String(x)+", y="+String(y)+" and leds["+String(p)+"]");
      leds[p] = stack[y][x];
    }
  }
  
}
